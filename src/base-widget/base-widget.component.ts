import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-base-widget',
  templateUrl: 'base-widget.component.html',
  styleUrls: ['./base-widget.component.css']
})

export class BaseWidgetComponent {

    public data: Array<any> = [
    {"related_persons":"Afrikaans"},
    {"related_persons":"العربية"},
    {"related_persons":"Беларуская"},
    {"related_persons":"Català"},
    {"related_persons":"Deutsch"},
    {"related_persons":"Español"},
    {"related_persons":"Esperanto"},
    {"related_persons":"Euskara"},
    {"related_persons":"Français"},
    {"related_persons":"हिन्दी"},
    {"related_persons":"Hrvatski"},
    {"related_persons":"Italiano"},
    {"related_persons":"Lietuvių"},
    {"related_persons":"Nederlands"},
    {"related_persons":"日本語"},
    {"related_persons":"Português"},
    {"related_persons":"Română"},
    {"related_persons":"Русский"},
    {"related_persons":"Slovenščina"}];

}
